package com.example.java_api_tc.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.java_api_tc.dto.CreateOrderRequest;
import com.example.java_api_tc.dto.Order;
import com.example.java_api_tc.dto.OrderStatus;
import com.example.java_api_tc.dto.Supplier;

import lombok.RequiredArgsConstructor;

@Repository
@RequiredArgsConstructor
public class OrderRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;


    public List<Order> findAllOrders() {
        return jdbcTemplate.query("SELECT * FROM \"order\"", this::mapOrder);
    }

    public void createOrder(CreateOrderRequest createOrderRequest) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update("""
                INSERT INTO "order" (supplier_id, status, drafted_on)
                VALUES (:supplier_id, :status, CURRENT_DATE)
                """, new MapSqlParameterSource()
                .addValue("supplier_id", createOrderRequest.getSupplierId())
                .addValue("status", OrderStatus.DRAFT.name()), keyHolder, new String[]{"id"});
        long id = keyHolder.getKey().longValue();
        jdbcTemplate.batchUpdate("""
                INSERT INTO order_item (order_id, product_id, order_quantity, stock_quantity, sold_quantity)
                VALUES (:order_id, :product_id, :order_quantity, :stock_quantity, :sold_quantity)
                """, createOrderRequest.getProducts().stream().map(request -> new MapSqlParameterSource()
                .addValue("order_id", id)
                .addValue("product_id", request.getProductId())
                .addValue("order_quantity", request.getOrderQuantity())
                .addValue("stock_quantity", request.getStockQuantity())
                .addValue("sold_quantity", request.getSoldQuantity())
        ).toArray(MapSqlParameterSource[]::new));
    }

    private Order mapOrder(ResultSet rs, int i) throws SQLException {
        return Order.builder()
                .withId(rs.getLong("id"))
                .withSupplier(Supplier.builder()
                        .withId(rs.getLong("supplier_id"))
                        .build())
                .withStatus(OrderStatus.valueOf(rs.getString("status")))
                .withDraftedOn(rs.getObject("drafted_on", LocalDate.class))
                .withPlacedOn(rs.getObject("placed_on", LocalDate.class))
                .withDeliveredOn(rs.getObject("delivered_on", LocalDate.class))
                .build();
    }

    @Transactional
    public void deleteOrderById(long id) {
        Map<String, Long> params = Map.of("order_id", id);
        jdbcTemplate.update("DELETE FROM order_item WHERE order_id = :order_id", params);
        jdbcTemplate.update("DELETE FROM \"order\" WHERE id = :order_id", params);
    }
}
