package com.example.java_api_tc.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import jakarta.validation.constraints.Positive;
import jakarta.validation.constraints.PositiveOrZero;
import lombok.Builder;
import lombok.Value;

@Value
@Builder(setterPrefix = "with", toBuilder = true)
@JsonDeserialize(builder = CreateOrderProductRequest.CreateOrderProductRequestBuilder.class)
public class CreateOrderProductRequest {

    @Positive
    long productId;

    @PositiveOrZero
    long soldQuantity;

    @PositiveOrZero
    long stockQuantity;

    @Positive
    long orderQuantity;

}
