package com.example.java_api_tc.dto;

public enum OrderStatus {
    DRAFT, PLACED, DELIVERED
}
