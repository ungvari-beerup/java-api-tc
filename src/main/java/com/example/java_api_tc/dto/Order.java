package com.example.java_api_tc.dto;

import java.time.LocalDate;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Value;

@Value
@Builder(setterPrefix = "with", toBuilder = true)
@JsonDeserialize(builder = Order.OrderBuilder.class)
@Schema(description = "An order from a single supplier")
public class Order {

    @Schema(description = "Unique ID of the order")
    long id;

    @Schema(description = "Date of the order creation")
    LocalDate draftedOn;

    @Schema(description = "Date of the order placement")
    LocalDate placedOn;

    @Schema(description = "Date of the order delivery")
    LocalDate deliveredOn;

    @Schema(description = "Supplier")
    Supplier supplier;

    @Schema(description = "Order status")
    OrderStatus status;

}
