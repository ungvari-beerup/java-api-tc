package com.example.java_api_tc.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import lombok.Builder;
import lombok.Value;

@Value
@Builder(setterPrefix = "with")
@JsonDeserialize(builder = Product.ProductBuilder.class)
public class Product {

    long id;
    String name;
    String unit;

}
