package com.example.java_api_tc.dto;

import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Value;

@Value
@Builder(setterPrefix = "with")
@JsonDeserialize(builder = ListResponse.ListResponseBuilder.class)
public class ListResponse<T> {

    @Schema(description = "List of items")
    @Builder.Default
    List<T> items = List.of();

}
