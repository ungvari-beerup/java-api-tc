package com.example.java_api_tc.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Value;

@Value
@Builder(setterPrefix = "with", toBuilder = true)
@JsonDeserialize(builder = OrderDetails.OrderDetailsBuilder.class)
@Schema(description = "Details of an order, possibly containing multiple products from a single supplier")
public class OrderDetails {

    @JsonUnwrapped
    Order order;

    @Schema(description = "Items of the products to order")
    @Builder.Default
    List<OrderItem> items = List.of();

}
