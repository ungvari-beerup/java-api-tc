package com.example.java_api_tc.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import lombok.Builder;
import lombok.Value;

@Value
@Builder(setterPrefix = "with")
@JsonDeserialize(builder = OrderItem.OrderItemBuilder.class)
public class OrderItem {

    Product product;
    long orderQuantity;
    long stockQuantity;
    long soldQuantity;

}
