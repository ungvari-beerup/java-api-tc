package com.example.java_api_tc.dto;

import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Positive;
import lombok.Builder;
import lombok.Value;

@Value
@Builder(setterPrefix = "with", toBuilder = true)
@JsonDeserialize(builder = CreateOrderRequest.CreateOrderRequestBuilder.class)
@Schema(description = "Order object used for drafting orders")
public class CreateOrderRequest {

    @Positive
    @Schema(description = "ID of a supplier.")
    long supplierId;

    @NotEmpty
    @Schema(description = "List of product objects needed for the order to be created")
    List<CreateOrderProductRequest> products;

}
