package com.example.java_api_tc.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import lombok.Builder;
import lombok.Value;

@Value
@Builder(setterPrefix = "with", toBuilder = true)
@JsonDeserialize(builder = Supplier.SupplierBuilder.class)
public class Supplier {

    long id;
    String name;
    String address;

}
