package com.example.java_api_tc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaApiTcApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaApiTcApplication.class, args);
	}

}
