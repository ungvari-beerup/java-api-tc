package com.example.java_api_tc.web;

import java.util.List;

import org.springframework.web.bind.annotation.RestController;

import com.example.java_api_tc.api.OrderApi;
import com.example.java_api_tc.dao.OrderRepository;
import com.example.java_api_tc.dto.CreateOrderRequest;
import com.example.java_api_tc.dto.ListResponse;
import com.example.java_api_tc.dto.Order;
import com.example.java_api_tc.dto.OrderDetails;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
public class OrderController implements OrderApi {

    private final OrderRepository repository;

    @Override
    public ListResponse<Order> list() {
        return ListResponse.<Order>builder()
                .withItems(repository.findAllOrders())
                .build();
    }

    @Override
    public OrderDetails details(long id) {
        return null;
    }

    @Override
    public void create(List<CreateOrderRequest> createOrderRequestList) {
        createOrderRequestList.forEach(repository::createOrder);
    }

    @Override
    public void placed(long id) {

    }

    @Override
    public void delivered(long id) {

    }

    @Override
    public void delete(long id) {
        repository.deleteOrderById(id);
    }

}
