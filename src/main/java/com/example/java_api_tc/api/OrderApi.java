package com.example.java_api_tc.api;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import com.example.java_api_tc.dto.CreateOrderRequest;
import com.example.java_api_tc.dto.ListResponse;
import com.example.java_api_tc.dto.Order;
import com.example.java_api_tc.dto.OrderDetails;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;

@RequestMapping("/api/orders")
@Tag(name = "OrderApi", description = "List, draft, update orders")
public interface OrderApi {

    @GetMapping(produces = APPLICATION_JSON_VALUE)
    @Operation(description = "List all orders sorted by date descending")
    ListResponse<Order> list();

    @GetMapping(value = "{id}", produces = APPLICATION_JSON_VALUE)
    @Operation(description = "Fetch order details by ID")
    @ApiResponses({@ApiResponse(responseCode = "404", description = "Order not found by the given ID")})
    OrderDetails details(@PathVariable("id") @Parameter(description = "Order ID", required = true) long id);

    @PostMapping(consumes = APPLICATION_JSON_VALUE)
    @Operation(description = "Create order draft")
    void create(@RequestBody @Valid List<CreateOrderRequest> createOrderRequestList);

    @PutMapping(value = "{id}/placed")
    @Operation(description = "Mark order as placed")
    @ApiResponses({
            @ApiResponse(responseCode = "404", description = "Order not found by the given ID"),
            @ApiResponse(responseCode = "409", description =
                    "Order already delivered, so it cannot be placed.")

    })
    void placed(@PathVariable("id") @Parameter(description = "Order ID", required = true) long id);

    @PutMapping(value = "{id}/delivered")
    @Operation(description = "Mark order as delivered")
    @ApiResponses({
            @ApiResponse(responseCode = "404", description = "Order not found by the given ID"),
            @ApiResponse(responseCode = "409", description =
                    "Order not placed yet, so it cannot be delivered.")
    })
    void delivered(@PathVariable("id") @Parameter(description = "Order ID", required = true) long id);

    @DeleteMapping(value = "{id}")
    @Operation(description = "Delete order")
    @ApiResponses({
            @ApiResponse(responseCode = "404", description = "Order not found by the given ID"),
            @ApiResponse(responseCode = "409", description =
                    "Order already placed or delivered. Only drafts can be deleted before placement.")
    })
    void delete(@PathVariable("id") @Parameter(description = "Order ID", required = true) long id);

}
