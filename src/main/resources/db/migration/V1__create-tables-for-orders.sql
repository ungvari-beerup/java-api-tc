CREATE TABLE "order" (
    id serial PRIMARY KEY,
    supplier_id integer NOT NULL,
    status varchar(100) NOT NULL,
    drafted_on date NOT NULL,
    placed_on date,
    delivered_on date
);

CREATE TABLE order_item (
    id serial PRIMARY KEY,
    order_id integer NOT NULL,
    product_id integer NOT NULL,
    order_quantity integer NOT NULL,
    stock_quantity integer NOT NULL,
    sold_quantity integer NOT NULL,
    CONSTRAINT order_item_order_id_fk FOREIGN KEY (order_id) REFERENCES "order" (id)
);
