package com.example.java_api_tc;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;

@Import(TestcontainersConfiguration.class)
@SpringBootTest
class JavaApiTcApplicationTests {

	@Test
	void contextLoads() {
	}

}
