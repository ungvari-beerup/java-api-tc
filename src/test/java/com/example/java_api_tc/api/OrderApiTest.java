package com.example.java_api_tc.api;

import java.time.LocalDate;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.context.annotation.Import;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.example.java_api_tc.TestcontainersConfiguration;
import com.example.java_api_tc.dto.CreateOrderProductRequest;
import com.example.java_api_tc.dto.CreateOrderRequest;
import com.example.java_api_tc.dto.ListResponse;
import com.example.java_api_tc.dto.Order;
import com.example.java_api_tc.dto.OrderDetails;
import com.example.java_api_tc.dto.OrderItem;
import com.example.java_api_tc.dto.OrderStatus;
import com.example.java_api_tc.dto.Product;
import com.example.java_api_tc.dto.Supplier;
import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import feign.Feign;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import feign.spring.SpringContract;

@Import(TestcontainersConfiguration.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class OrderApiTest {

    private static final long SUPPLIER_ID_1 = 1L;
    private static final String SUPPLIER_NAME_1 = "Best Toys";
    private static final String SUPPLIER_ADDRESS_1 = "12345 Wonderland, 221B Baker Street";

    private static final long PRODUCT_ID_1 = 1L;
    private static final String PRODUCT_NAME_1 = "Ferrari 488 GTE toy car";
    private static final String PRODUCT_UNIT_1 = "pcs";

    private static final long ORDER_QUANTITY_1 = 3L;
    private static final long SOLD_QUANTITY_PRODUCT_1 = 3L;
    private static final long STOCK_QUANTITY_PRODUCT_1 = 2L;

    private OrderApi orderApi;

    @LocalServerPort
    private int port;

    @BeforeAll
    void setUp() {
        JavaTimeModule javaTimeModule = new JavaTimeModule();
        List<Module> modules = List.of(javaTimeModule);
        orderApi = Feign.builder()
                .contract(new SpringContract())
                .encoder(new JacksonEncoder(modules))
                .decoder(new JacksonDecoder(modules))
                .target(OrderApi.class, "http://localhost:" + port);
    }

    @DisplayName("Given there are no orders drafted yet " +
            "When order list retrieved " +
            "Then the result is an empty list " +
            "And HTTP status is 200 OK.")
    @Test
    void testOrderListIsEmptyByDefault() throws Exception {
        ListResponse<Order> list = orderApi.list();
        assertNotNull(list, "Order list is never null");
        assertTrue(list.getItems().isEmpty(), "Order list is empty");
    }

    @DisplayName("Given there are no orders drafted yet " +
            "When a single order is created with a single item " +
            "Then it is persisted " +
            "And HTTP status is 200 OK.")
    @Test
    void testSingleOrderDraftingSuccessful() {
        orderApi.create(List.of(CreateOrderRequest.builder()
                .withSupplierId(SUPPLIER_ID_1)
                .withProducts(List.of(
                        CreateOrderProductRequest.builder()
                                .withProductId(PRODUCT_ID_1)
                                .withOrderQuantity(ORDER_QUANTITY_1)
                                .withSoldQuantity(SOLD_QUANTITY_PRODUCT_1)
                                .withStockQuantity(STOCK_QUANTITY_PRODUCT_1)
                                .build()))
                .build()));

        List<Order> items = orderApi.list().getItems();
        assertEquals(1, items.size());
        Order order = items.get(0);
        long orderId = order.getId();
        assertTrue(orderId != 0L, "Non-default identifier is given to the order");
        checkOrder1ListAttributes(order);

        OrderDetails details = orderApi.details(orderId);
        //FIXME: checkOrder1Details(details);

        // clean-up
        orderApi.delete(orderId);
        ListResponse<Order> list = orderApi.list();
        assertTrue(list.getItems().isEmpty(), "Order list is empty");
    }

    private static void checkOrder1ListAttributes(Order order) {
        assertEquals(OrderStatus.DRAFT, order.getStatus(), "Order is drafted");
        assertEquals(LocalDate.now(), order.getDraftedOn(), "Order drafted today");
        assertNull(order.getPlacedOn(), "Order not yet placed");
        assertNull(order.getDeliveredOn(), "Order not yet delivered");
        Supplier supplier = order.getSupplier();
        assertEquals(SUPPLIER_ID_1, supplier.getId(), "Supplier ID is persisted");
        //FIXME: assertEquals(SUPPLIER_NAME_1, supplier.getName(), "Supplier name is correct");
        //FIXME: assertEquals(SUPPLIER_ADDRESS_1, supplier.getAddress(), "Supplier address is correct");
    }

    private static void checkOrder1Details(OrderDetails details) {
        checkOrder1ListAttributes(details.getOrder());

        List<OrderItem> orderItems = details.getItems();
        assertEquals(1, orderItems.size(), "Single order item is present");
        OrderItem orderItem = orderItems.get(0);
        assertEquals(ORDER_QUANTITY_1, orderItem.getOrderQuantity(), "Order quantity is persisted");
        assertEquals(SOLD_QUANTITY_PRODUCT_1, orderItem.getSoldQuantity(), "Sold quantity is persisted");
        assertEquals(STOCK_QUANTITY_PRODUCT_1, orderItem.getStockQuantity(), "Stock quantity is persisted");

        Product product = orderItem.getProduct();
        assertEquals(PRODUCT_ID_1, product.getId(), "Ordered product ID is persisted");
        assertEquals(PRODUCT_NAME_1, product.getName(), "Product name is correct");
        assertEquals(PRODUCT_UNIT_1, product.getUnit(), "Product unit is correct");
    }

}
