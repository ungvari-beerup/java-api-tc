package com.example.java_api_tc;

import org.springframework.boot.SpringApplication;

public class TestJavaApiTcApplication {

	public static void main(String[] args) {
		SpringApplication.from(JavaApiTcApplication::main).with(TestcontainersConfiguration.class).run(args);
	}

}
